#ifndef __I_DEBUG_BITS_HH_
#define __I_DEBUG_BITS_HH_

#include <bitset>
#include <climits>
#include <cstddef>

namespace debug {
template <typename _Tp>
struct bitsize {
    constexpr static size_t value = sizeof(_Tp) * CHAR_BIT;
};

template <typename _Tp>
using bitset = std::bitset<bitsize<_Tp>::value>;
} // namespace debug

#endif
