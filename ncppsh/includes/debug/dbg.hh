#ifndef __I_DEBUG_DBG_HH_
#define __I_DEBUG_DBG_HH_

#include <iostream>
#include <ostream>
#include <source_location>

namespace debug {
template <typename _Tp>
struct __debug_fetcher {
    static _Tp __impl(_Tp);
};

template <>
struct __debug_fetcher<void*> {
    static void* __impl(void* ptr) {
        return ptr;
    }
};

template <typename _Tp>
struct __debug_fetcher<_Tp&> {
    static _Tp& __impl(_Tp& ref) {
        return ref;
    }
};

template <typename _Tp>
struct __debug_fetcher<_Tp*> {
    static _Tp const& __impl(_Tp* ptr) {
        return *ptr;
    }
};

template <typename _Tp>
concept _ImplOstream = requires(std::ostream& out, _Tp const& a) {
    { out << a } -> std::same_as<decltype(out)>;
};

template <_ImplOstream _Tp>
struct __debug {
    static _Tp __impl(char const[], _Tp,
                      std::source_location = std::source_location::current());
};

template <_ImplOstream _Tp>
_Tp __debug<_Tp>::__impl(char const name[], _Tp xvalue,
                         std::source_location loc) {
    std::clog << loc.file_name() << ": " << loc.function_name()
              << ": col: " << loc.column() << ", row: " << loc.line() << " ["
              << name << " = " << __debug_fetcher<_Tp>::__impl(xvalue)
              << "]\r\n"
              << std::flush;
    return xvalue;
}

#define X(tdecl, rtype)                                                        \
    template <tdecl>                                                           \
    struct __debug<rtype> {                                                    \
        static rtype                                                           \
        __impl(char const name[], rtype xvalue,                                \
               std::source_location loc = std::source_location::current()) {   \
            std::clog << loc.file_name() << ": " << loc.function_name()        \
                      << ": col: " << loc.column() << ", row: " << loc.line()  \
                      << " [" << name << " = "                                 \
                      << __debug_fetcher<rtype>::__impl(xvalue) << "]\r\n"     \
                      << std::flush;                                           \
            return xvalue;                                                     \
        }                                                                      \
    };

X(_ImplOstream _Tp, _Tp&)
X(_ImplOstream _Tp, _Tp*)
X(, void*)

#define __dbg_impl(name, value) __debug<decltype(value)>::__impl(name, value)

#define dbg(lvalue)   __dbg_impl(#lvalue, lvalue)
#define dbgrv(rvalue) __dbg_impl("", rvalue)

#undef X
} // namespace debug

#endif
