#ifndef __I_NCPPSH_EXCEPTIONS_HH_
#define __I_NCPPSH_EXCEPTIONS_HH_

#include <cstring>
#include <exception>
#include <string>

namespace ncppsh {

/**!
 * @brief Exception raised when a bad file is passed
 */
class badfile_exception final : public std::exception {
public:
    badfile_exception(char* const fpath)
        : std::exception{}
        , filename(fpath){};

    badfile_exception(std::string const& fpath)
        : std::exception{}
        , filename(fpath){};

    virtual char const* what() const noexcept override {
        std::string buffer = "bad file path: ";
        char*       alloc = new char[buffer.length() + this->filename.length()];

        std::strcpy(alloc, buffer.c_str());
        std::strcpy(alloc + buffer.length(), this->filename.c_str());

        return alloc;
    }

private:
    std::string filename; ///< path to file causing the exception
};

/**!
 * @brief Exception is raised when the shell state is uninitialized
 */
class uninitialized_state final : public std::exception {
public:
    uninitialized_state()
        : std::exception{} {}

    virtual char const* what() const noexcept override {
        return "state is uninitialized";
    }
};
} // namespace ncppsh

#endif // !__I_NCPPSH_EXCEPTIONS_HH_
