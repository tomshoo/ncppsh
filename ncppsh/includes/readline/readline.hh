#ifndef __I_NCPPSH_READLINE_READLINE_HH_
#define __I_NCPPSH_READLINE_READLINE_HH_

#include <cstdint>
#include <istream>
#include <string>

namespace ncppsh::rl {
enum class renderer_output_mode {
    colored,
    uncolored,
};

struct __attribute__((__packed__)) line_state {
    unsigned interrupted : 1;
    unsigned end_of_file : 1;
    unsigned line_escape : 1;

    unsigned : 5;

    explicit line_state(uint8_t);
    explicit operator uint8_t();
};

class readline {
public:
    /**
     * @brief constructs a readline instance that is responsible to close or
     *        free the `istream` pointer by itself
     *
     * @param streamptr takes the pointer to the input stream `istream` and owns
     *                  it, i.e, `readline` is responsible for appropriately
     *                  closing and freeing the stream
     *
     * @note It is marked as `nothrow` as the instanciation of the stream is the
     *       responsible of the caller
     */
    readline(std::istream*) noexcept;
    virtual line_state getline(std::string&);
    virtual ~readline();

protected:
    std::istream* streamptr;
};
} // namespace ncppsh::rl

#endif