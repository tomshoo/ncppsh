#ifndef __I_NCPPSH_READLINE_TTY_READLINE_HH_
#define __I_NCPPSH_READLINE_TTY_READLINE_HH_

#include "readline/readline.hh"
#include "renderer/direction.hh"
#include "renderer/style.hh"
#include <termkey.h>

#include <termios.h>

namespace ncppsh::rl {

class tty_readline : public readline {
public:
    tty_readline(renderer::color_mode = renderer::color_mode::NOCOLOR);

    void       restore_terminal(void);
    line_state getline(std::string&) override;

private:
    char getch();
    void handle_key_sym_event(TermKeyKey);
    void handle_directional_key(renderer::direction_t, bool = false);

    TermKey*       tk;
    struct termios p;

    renderer::color_mode style;
};
} // namespace ncppsh::rl

#endif
