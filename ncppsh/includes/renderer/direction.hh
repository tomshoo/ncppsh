#ifndef __I_RENDERER_DIRECTION_HH_
#define __I_RENDERER_DIRECTION_HH_

#include <cstdint>
#include <ostream>

namespace renderer {
#define _DIRECTION_DECL                                                                            \
    X(UP, = 65)                                                                                    \
    X(DOWN, )                                                                                      \
    X(LEFT, )                                                                                      \
    X(RIGHT, )

enum class direction_t : uint8_t {
#define X(dname, dvalue) dname dvalue,
    _DIRECTION_DECL
#undef X
};

inline std::ostream& operator<<(std::ostream& out, direction_t const& d) {
    switch (d) {
#define X(dname, _)                                                                                \
    case direction_t::dname: return out << #dname;
        _DIRECTION_DECL
#undef X
        default: return out << "UNKNOWN";
    }
}
#undef _DIRECTION_DECL
} // namespace renderer

#endif // !__I_RENDERER_DIRECTION_HH_
