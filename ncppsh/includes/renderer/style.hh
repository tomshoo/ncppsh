#ifndef __I_RENDERER_STYLE_HH_
#define __I_RENDERER_STYLE_HH_

#include <cstdint>
#include <ostream>

namespace renderer {
#define _X_COLOR_MODE_DECL                                                     \
    X(COLOR)                                                                   \
    X(NOCOLOR)                                                                 \
    X(RAW)

enum class color_mode : uint8_t {
#define X(cmode) cmode,
    _X_COLOR_MODE_DECL
#undef X
};

inline std::ostream& operator<<(std::ostream& out, color_mode const& mode) {
    switch (mode) {
#define X(mode)                                                                \
    case color_mode::mode: return out << #mode;
        _X_COLOR_MODE_DECL
#undef X
        default: return out << "UNKNOWN";
    }
}

#undef _X_COLOR_MODE_DECL
} // namespace renderer

#endif // !__I_RENDERER_STYLE_HH_
