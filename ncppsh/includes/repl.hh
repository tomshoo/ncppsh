#ifndef __I_NCPPSH_REPL_HH_
#define __I_NCPPSH_REPL_HH_

#include <istream>

namespace ncppsh {
class repl {
public:
    static repl from_file();
    static repl interactive();

    ~repl();

private:
    std::istream* stream;

    repl(std::istream*);
};
} // namespace ncppsh

#endif
