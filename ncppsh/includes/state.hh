#ifndef __I_NCPPSH_STATE_HH_
#define __I_NCPPSH_STATE_HH_

#include <mutex>
#include <shared_mutex>
#include <string>
#include <unordered_map>
namespace ncppsh {

class state;
class state_reader;
class state_writer;

class state {
public:
    std::unordered_map<std::string, std::string> local_vars;

    static void         initialize();
    static state_reader reader();
    static state_writer writer();

    ~state();

private:
    state();

    std::shared_mutex mutex;
    static state*     instance;
};

class state_reader {
public:
    state const& operator*();
    state const* operator->();

    ~state_reader() = default;

    friend class state;

private:
    state_reader(std::shared_mutex&, state const*);
    state_reader(state_reader const&) = delete;
    state_reader(state_reader&&)      = delete;

    std::shared_lock<std::shared_mutex> _lock;
    state const*                        instance;
};

class state_writer {
public:
    state& operator*();
    state* operator->();

    ~state_writer() = default;

    friend class state;

private:
    state_writer(std::shared_mutex&, state*);
    state_writer(state_writer const&) = delete;
    state_writer(state_writer&&)      = delete;

    std::unique_lock<std::shared_mutex> _lock;
    state*                              instance;
};

} // namespace ncppsh

#endif
