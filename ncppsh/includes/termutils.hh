#ifndef __I_TERMUTILS_HH_
#define __I_TERMUTILS_HH_

#include <sys/ioctl.h>
#include <termkey.h>

/**
 * @brief gets the size of the currently active terminal window
 * @note  this function is not marked as `nothrow` as it has side effects and
 *        can be a possible cancelation point.
 */
extern struct winsize getwinsize() noexcept(false);

/**
 * @brief gets a single key event from stdin stream
 * @note  it is the responsibility of the caller to enter and exit raw mode
 */
#endif
