#include "exceptions.hh"
#include "readline/readline.hh"
#include "readline/tty_readline.hh"
#include "state.hh"

#include <cstdio>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>

#include <string.h>
#include <sys/ioctl.h>
#include <termkey.h>
#include <unistd.h>

using ncppsh::rl::readline;
using ncppsh::rl::tty_readline;

readline* load_file(std::filesystem::path);
readline* load_stdin();

int main(int const argc, char** const argv) {
    TERMKEY_CHECK_VERSION;
    ncppsh::state::initialize();

    std::string                base_exe = basename(*argv);
    std::optional<std::string> fpath    = std::nullopt;

    for (int opt; (opt = getopt(argc, argv, ":hr:")) != EOF;) {
        switch (opt) {
            case 'r': fpath = optarg; break;
            case 'h': std::cout << "usage: " << base_exe << " [-hr]" << std::endl; return 0;
            case '?':
                std::cerr << "error: " << base_exe << ": unknown argument -" << (char)optopt
                          << std::endl;
                return 2;
            default:
                std::cerr << "error: " << base_exe << ": argument requires a value: -"
                          << (char)optopt << std::endl;
                return 2;
        }
    }

    try {
        std::string               buffer;
        std::unique_ptr<readline> rl{(fpath) ? load_file(*fpath) : load_stdin()};

        buffer.reserve(1024);

        while (not rl->getline(buffer).end_of_file) {
            std::cout << buffer << std::endl;
        }
    } catch (ncppsh::badfile_exception const& excpt) {
        std::cerr << "error: " << excpt.what() << std::endl;
        return 1;
    }

    return 0;
}

readline* load_file(std::filesystem::path path) {
    auto original_path =
        std::filesystem::is_symlink(path) ? std::filesystem::read_symlink(path) : path;

    if (not std::filesystem::is_regular_file(path))
        throw ncppsh::badfile_exception{path};

    auto fstream = new std::ifstream{path.c_str()};
    if (not fstream->good())
        throw ncppsh::badfile_exception{path};

    auto rl = new readline{fstream};
    return rl;
}

readline* load_stdin() {
    auto rl = isatty(fileno(stdin)) ? new tty_readline{} : new readline{&std::cin};

    return rl;
}
