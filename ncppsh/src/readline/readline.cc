#include "readline/readline.hh"

#include <bit>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <istream>
#include <string>

using namespace ncppsh::rl;

line_state::line_state(uint8_t bits) {
    auto ptr = (uint8_t*)this;
    *ptr     = bits;
}

line_state::operator uint8_t() {
    return std::bit_cast<uint8_t>(*this);
}

readline::readline(std::istream* stream) noexcept
    : streamptr(stream) {}

line_state readline::getline(std::string& buffer) {
    line_state state = (line_state)0;
    std::getline(*(this->streamptr), buffer);

    if (this->streamptr->eof())
        state.end_of_file = 1;

    return state;
}

readline::~readline() {
    auto ptr = this->streamptr;

    if (ptr == &std::cin)
        return;

    if (auto fptr = dynamic_cast<std::ifstream*>(ptr)) {
        fptr->close();
    }

    delete ptr;
}