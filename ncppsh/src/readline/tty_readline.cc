#include "readline/tty_readline.hh"
#include "debug/bits.hh"
#include "readline/readline.hh"
#include "renderer/style.hh"

#include <csignal>
#include <cstdio>
#include <iostream>
#include <istream>

#include <memory>
#include <ostream>
#include <sys/termios.h>
#include <termios.h>
#include <termkey.h>
#include <unistd.h>

using namespace ncppsh::rl;

tty_readline::tty_readline(renderer::color_mode style)
    : readline(&std::cin)
    , style(style) {
    this->tk = termkey_new(fileno(stdin), TERMKEY_FLAG_NOTERMIOS);
    tcgetattr(fileno(stdin), &this->p);
}

void tty_readline::restore_terminal() {
    tcsetattr(fileno(stdin), TCSANOW, &this->p);
}

line_state tty_readline::getline(std::string& buffer) {
    std::shared_ptr<void> _scope_guard = {nullptr, [&](...) noexcept { this->restore_terminal(); }};

    struct termios newp  = this->p;
    line_state     state = (line_state)0;
    auto           input = [&]() {
        char ch           = this->getch();
        state.end_of_file = this->streamptr->eof();
        return ch;
    };

    newp.c_iflag &= ~(IXON | ICRNL | ISTRIP);
    newp.c_lflag &= ~(ECHO | ICANON | ISIG | IEXTEN);

    if (this->streamptr->eof()) {
        state.end_of_file = 1;
        return state;
    }

    tcsetattr(fileno(stdin), TCSANOW, &newp);
    buffer.clear();

    for (char ch = 0; (ch = input()) != 0xD && not state.end_of_file;) {
        if (not ch)
            continue;

        std::cout << ch << std::flush;
        buffer.push_back(ch);
    }

    return state;
}

void tty_readline::handle_directional_key(renderer::direction_t d, bool extended) {
    std::cout << "directional key: " << d << ", extender: " << std::boolalpha << extended
              << std::noboolalpha << "\r\n"
              << std::flush;
}

void tty_readline::handle_key_sym_event(TermKeyKey key) {
    switch (key.code.sym) {
        case TERMKEY_SYM_UP:
        case TERMKEY_SYM_DOWN:
        case TERMKEY_SYM_LEFT:
        case TERMKEY_SYM_RIGHT:
            this->handle_directional_key((renderer::direction_t)(key.code.sym + 58));
            break;

        case TERMKEY_SYM_HOME:
        case TERMKEY_SYM_END:
        case TERMKEY_SYM_PAGEUP:
        case TERMKEY_SYM_PAGEDOWN:
            this->handle_directional_key((renderer::direction_t)(key.code.sym + 49), true);
            break;
        default: break;
    }
}

char tty_readline::getch() {
    TermKeyKey key    = {};
    auto       result = termkey_waitkey(this->tk, &key);

    switch (result) {
        case TERMKEY_RES_EOF: this->streamptr->setstate(std::ios::eofbit); return 0;

        case TERMKEY_RES_KEY:
            if (key.type == TERMKEY_TYPE_KEYSYM) {
                this->handle_key_sym_event(key);
                break;
            }

            if (key.modifiers == TERMKEY_KEYMOD_CTRL && key.code.codepoint == 'c') {
                this->streamptr->setstate(std::ios::eofbit);
                kill(getpid(), SIGINT);
            }

            if (key.modifiers == TERMKEY_KEYMOD_CTRL && key.code.codepoint == 'd') {
                this->streamptr->setstate(std::ios::eofbit);
                return 0;
            }

            std::cout << debug::bitset<int>(key.modifiers) << std::endl;
        default: break;
    }

    return 0;
}
