#include <iostream>
#include <shared_mutex>
#include <state.hh>

#include "exceptions.hh"

using namespace ncppsh;

state* state::instance = nullptr;
state::state() {}

void state::initialize() {
    if (state::instance) {
        std::clog << "warning: state is already initialized" << std::endl;
        return;
    }

    state::instance = new state;
}

#define X(actor)                                                               \
    state_##actor state::actor() {                                             \
        if (not state::instance)                                               \
            throw uninitialized_state();                                       \
                                                                               \
        auto& mutex = state::instance->mutex;                                  \
        return state_##actor{mutex, state::instance};                          \
    }

X(reader)
X(writer)
#undef X

state_reader::state_reader(std::shared_mutex& mutex, state const* ptr)
    : _lock(mutex)
    , instance(ptr) {}

state_writer::state_writer(std::shared_mutex& mutex, state* ptr)
    : _lock(mutex)
    , instance(ptr) {}

state const& state_reader::operator*() {
    return *this->instance;
}

state& state_writer::operator*() {
    return *this->instance;
}

state const* state_reader::operator->() {
    return this->instance;
}

state* state_writer::operator->() {
    return this->instance;
}
