#include "termutils.hh"

#include <cerrno>
#include <cstdio>
#include <cstdlib>

#include <sys/ioctl.h>
#include <sys/termios.h>
#include <unistd.h>

struct winsize getwinsize() noexcept(false) {
    struct winsize ws;
    ioctl(fileno(stdout), TIOCGWINSZ, &ws);

    if (not errno)
        return ws;

    int err = errno;
    perror("ioctl: invalid io device");
    exit(err);
}
